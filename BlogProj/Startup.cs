﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BlogProj.Startup))]
namespace BlogProj
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
