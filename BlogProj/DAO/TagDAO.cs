﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using BlogProj.Exceptions;
using BlogProj.Models;

namespace BlogProj.DAO
{
    public class TagDAO : ITagDAO
    {
        private ContextEf context;


        public TagDAO()
        {
            this.context = new ContextEf();
        }

        public ICollection<Tag> FindAll()
        {
            return context.Tags.ToList();
        }

        public Tag FindOne(long? id)
        {
            Tag tag = context.Tags.Find(id);
            return tag;
        }

        public bool Ajouter(Tag tag)
        {
            int lineAffected = 0;
            try
            {
                context.Tags.Add(tag);
                lineAffected = context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw new DuplicateEntityException("Value already exist");
            }
            return lineAffected > 0 ? true : false;
        }

        public bool Edit(Tag tag)
        {
            int lineAffected = 0;
            try
            {
                context.Entry(tag).State = EntityState.Modified;
                lineAffected = context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw new DuplicateEntityException("Value already exist");
            }
            return lineAffected > 0 ? true : false;
        }

        public bool Delete(long? id)
        {
            try
            {
                Tag tag = FindOne(id);
                context.Tags.Remove(tag);
                int lineaffected = context.SaveChanges();
                return lineaffected > 0 ? true : false;
            }
            catch (DataException)
            {
                throw new DeleteException("Erreur lors de la suppression, try later");
            }
        }

        public ICollection<Tag> GetTagsByTerm(string term)
        {
            return context.Tags.Where(t => t.Nom.Contains(term)).ToList();
        }
    }
}