﻿using BlogProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogProj.DAO
{
    public interface IAnnonceDAO
    {
        ICollection<Annonce> FindAll();

        Annonce FindById(long? id);

        bool Ajouter(Annonce annonce);

        bool Edit(Annonce annonce);

        ICollection<Annonce> FindByAuteurId(long? id);
    }
}
