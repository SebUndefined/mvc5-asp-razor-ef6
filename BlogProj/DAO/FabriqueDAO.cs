﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogProj.DAO
{
    public static class FabriqueDAO
    {
        public static IAnnonceDAO GetAnnconceDAO()
        {
            return new AnnonceDAO();
        }

        public static ITagDAO GetTagDAO()
        {
            return new TagDAO();
        }

        public static IAuteurDAO GetAuteurDAO()
        {
            return new AuteurDAO();
        }
        public static ICompetenceDAO GetComptenceDAO()
        {
            return new CompetenceDAO();
        }
    }
}