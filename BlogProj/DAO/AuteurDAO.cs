﻿using BlogProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogProj.DAO
{
    public class AuteurDAO : IAuteurDAO
    {

        private ContextEf context;

        public AuteurDAO()
        {
            this.context = new ContextEf();
        }

        public bool Ajouter(Auteur auteur)
        {
            context.Auteur.Add(auteur);
            int line = context.SaveChanges();
            return line > 0 ? true : false;
        }

        public bool Delete(long? id)
        {
            throw new NotImplementedException();
        }

        public bool Edit(Auteur auteur)
        {
            throw new NotImplementedException();
        }

        public ICollection<Auteur> FindAll()
        {
            return context.Auteur.ToList();
        }

        public Auteur FindOne(long? id)
        {
            return context.Auteur.Find(id);
        }
    }
}