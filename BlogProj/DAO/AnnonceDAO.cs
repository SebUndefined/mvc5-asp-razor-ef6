﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BlogProj.Models;

namespace BlogProj.DAO
{
    public class AnnonceDAO : IAnnonceDAO
    {
        private ContextEf context;

        public AnnonceDAO()
        {
            this.context = new ContextEf();
        }

        public ICollection<Annonce> FindAll()
        {
            return context.Annonces.ToList();
        }

        public Annonce FindById(long? id)
        {
            Annonce annonceResult = context.Annonces
                .Where(annonce => annonce.AnnonceId == id).Include(a => a.Tags)
                .Include(a => a.AnnonceCompetences.Select(ac => ac.Competence))
                .FirstOrDefault();
            return annonceResult;
        }

        public bool Ajouter(Annonce annonce)
        {
            
            foreach (Tag tag in annonce.Tags)
            {
                if (context.Entry(tag).State == EntityState.Detached)
                {
                    context.Tags.Attach(tag);
                }
            }
            foreach(AnnonceCompetence annonceCompetence in annonce.AnnonceCompetences)
            {
                if (context.Entry(annonceCompetence.Competence).State == EntityState.Detached)
                {
                    context.Competences.Attach(annonceCompetence.Competence);
                }
            }
            context.Annonces.Add(annonce);
            int line = context.SaveChanges();
            return line > 0 ? true : false;
        }

        public bool Edit(Annonce annonce)
        {
            //Annonce annonceDb = context.Annonces.FirstOrDefault(a => a.AnnonceId == annonce.AnnonceId);
            //Find existing annonce
            var annonceDb = context.Annonces.Include("Tags")
                .Where(a => a.AnnonceId == annonce.AnnonceId).FirstOrDefault<Annonce>();
            /* 2- Find deleted courses from student's course collection by 
            students' existing courses (existing data from database) minus students' 
            current course list (came from client in disconnected scenario) */
            //var deletedTag = annonceDb.Tags.Except(annonce.Tags).ToList<Tag>();
            var deletedTag = annonceDb.Tags.Where(l2 =>!annonce.Tags.Any(l1 => l1.Nom == l2.Nom)).ToList();
            var addedTags = annonce.Tags.Where(l2 =>!annonceDb.Tags.Any(l1 => l1.Nom == l2.Nom)).ToList();
            //var addedTags = annonce.Tags.Except(annonceDb.Tags).ToList<Tag>();
            deletedTag.ForEach(t => annonceDb.Tags.Remove(t));
            //8- Save changes which will reflect in StudentCourse table only
            
            foreach (Tag tag in addedTags)
            {
                if(context.Entry(tag).State == EntityState.Detached)
                {
                    context.Tags.Attach(tag);
                }
                annonceDb.Tags.Add(tag);
            }
            annonceDb.MiseAJour = DateTime.Now;
            annonceDb.DateCreation = DateTime.Now;
            context.SaveChanges();
            return true;
        }

        public ICollection<Annonce> FindByAuteurId(long? id)
        {
            ICollection<Annonce> annonces = context.Annonces.Where(a => a.AuteurId == id).ToList();
            return annonces;
        }
    }
}