﻿using BlogProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogProj.DAO
{
    public interface ICompetenceDAO
    {
        ICollection<Competence> FindAll();

        Competence FindOneByDenomination(string denomination);

        bool Add(Competence competence);
    }
}
