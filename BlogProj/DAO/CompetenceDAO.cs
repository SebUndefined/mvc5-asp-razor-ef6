﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlogProj.Models;

namespace BlogProj.DAO
{
    public class CompetenceDAO : ICompetenceDAO
    {
        private ContextEf context;

        public CompetenceDAO()
        {
            this.context = new ContextEf();
        }

        public ICollection<Competence> FindAll()
        {
            return context.Competences.ToList();
        }

        public bool Add(Competence competence)
        {
            context.Competences.Add(competence);
            int lineAffected = 0;
            lineAffected = context.SaveChanges();
            return lineAffected > 0 ? true : false;
        }

        public Competence FindOneByDenomination(string denomination)
        {
            return context.Competences.Where(c => c.Denomination.Equals(denomination)).FirstOrDefault();
        }
    }
}