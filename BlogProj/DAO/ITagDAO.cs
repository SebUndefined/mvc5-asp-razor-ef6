﻿using BlogProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogProj.DAO
{
    public interface ITagDAO
    {
        ICollection<Tag> FindAll();

        Tag FindOne(long? id);

        bool Ajouter(Tag tag);

        bool Edit(Tag tag);

        bool Delete(long? id);

        ICollection<Tag> GetTagsByTerm(string term);
    }
}
