﻿using BlogProj.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BlogProj.DAO
{
    public class ContextEf : DbContext
    {
        public ContextEf() : base("name=ConnectionDBAnnonce")
        {
            Database.SetInitializer<ContextEf>(new DropCreateDatabaseIfModelChanges<ContextEf>());
        }
        public DbSet<Annonce> Annonces { get; set; }
        public DbSet<Competence> Competences { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Auteur> Auteur { get; set; }
    }
}