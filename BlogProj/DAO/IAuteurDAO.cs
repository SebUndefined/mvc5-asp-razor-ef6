﻿using BlogProj.Models;
using System.Collections.Generic;

namespace BlogProj.DAO
{
    public interface IAuteurDAO
    {
        ICollection<Auteur> FindAll();

        Auteur FindOne(long? id);

        bool Ajouter(Auteur auteur);

        bool Edit(Auteur auteur);

        bool Delete(long? id);
    }
}