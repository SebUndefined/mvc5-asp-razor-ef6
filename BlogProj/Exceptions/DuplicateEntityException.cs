﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogProj.Exceptions
{
    public class DuplicateEntityException : Exception
    {
        public DuplicateEntityException()
        {
        }

        public DuplicateEntityException(string message) : base(message)
        {
        }
    }
}