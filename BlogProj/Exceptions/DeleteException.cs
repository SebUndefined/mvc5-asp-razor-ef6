﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogProj.Exceptions
{
    public class DeleteException : Exception
    {
        public DeleteException()
        {
        }

        public DeleteException(string message) : base(message)
        {
        }
    }
}