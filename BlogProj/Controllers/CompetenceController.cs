﻿using BlogProj.DAO;
using BlogProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogProj.Controllers
{
    [RoutePrefix("competence")]
    [Route("{action=index}")]
    public class CompetenceController : Controller
    {
        // GET: Competence
        [Route("")]
        public ActionResult Index()
        {
            ICompetenceDAO competenceDAO = FabriqueDAO.GetComptenceDAO();
            ICollection<Competence> competences = competenceDAO.FindAll();
            return View(competences);
        }

        [Route("add")]
        public ActionResult Create()
        {
            return View();
        }

        [Route("add")]
        [HttpPost]
        public ActionResult Create([Bind(Include = "CompetenceId,Denomination")]Competence competence)
        {
            if (ModelState.IsValid)
            {
                ICompetenceDAO competenceDAO = FabriqueDAO.GetComptenceDAO();
                competenceDAO.Add(competence);
                return RedirectToAction("index");
            }
            else
            {
                return View(competence);
            }
            
        }
    }
}