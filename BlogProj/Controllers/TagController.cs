﻿using BlogProj.DAO;
using BlogProj.Exceptions;
using BlogProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BlogProj.Controllers
{
    [RoutePrefix("tag")]
    [Route("{action=index}")]
    public class TagController : Controller
    {
        // GET: Tag
        [Route("")]
        public ActionResult Index()
        {
            ITagDAO tagDAO = FabriqueDAO.GetTagDAO();
            ICollection<Tag> tags = tagDAO.FindAll();
            return View(tags);
        }

        [HttpGet]
        [Route("add")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("add")]
        public ActionResult Create([Bind(Include = "TagId,Nom")] Tag tag)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITagDAO tagDAO = FabriqueDAO.GetTagDAO();
                    bool result = tagDAO.Ajouter(tag);
                    return RedirectToAction("index");
                }
                else
                {
                    return View(tag);
                }
            }
            catch (DuplicateEntityException ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(tag);
            }
        }
        [HttpGet]
        [Route("edit/{id:long}")]
        public ActionResult Edit(long? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITagDAO tagDAO = FabriqueDAO.GetTagDAO();
            Tag tag = tagDAO.FindOne(id);
            if (tag == null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("edit/{id:long}")]
        public ActionResult Edit([Bind(Include = "TagId,Nom")] Tag tag)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ITagDAO tagDAO = FabriqueDAO.GetTagDAO();
                    bool result = tagDAO.Edit(tag);
                    return RedirectToAction("index");
                }
                else
                {
                    return View(tag);
                }
            }
            catch (DuplicateEntityException ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(tag);
            }
        }

        // GET: tag/delete/5
        [Route("delete/{id}")]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ITagDAO tagDAO = FabriqueDAO.GetTagDAO();
            Tag tag = tagDAO.FindOne(id);
            if (tag == null)
            {
                return HttpNotFound();
            }
            return View(tag);
        }


        [Route("delete/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                ITagDAO tagDAO = FabriqueDAO.GetTagDAO();
                bool result = tagDAO.Delete(id);
                return RedirectToAction("index");
            }
            catch (DeleteException ex)
            {
                return RedirectToAction("index");
            }
        }
        [Route("gettags")]
        public ActionResult GetTags(string term)
        {
            ITagDAO tagDAO = FabriqueDAO.GetTagDAO();
            return Json(tagDAO.GetTagsByTerm(term).Select(t => new { label = t.Nom }), JsonRequestBehavior.AllowGet);
        }
    }
}