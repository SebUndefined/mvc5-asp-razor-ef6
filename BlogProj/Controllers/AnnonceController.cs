﻿using BlogProj.DAO;
using BlogProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace BlogProj.Controllers
{
    [RoutePrefix("annonce")]
    [Route("{action=index}")]
    public class AnnonceController : Controller
    {
        // GET: Annonce
        [Route("")]
        public ActionResult Index(string sortOrder, int? page)
        {
            IAnnonceDAO annonceDAO = FabriqueDAO.GetAnnconceDAO();
            var annonces = annonceDAO.FindAll();
            
            ViewBag.TitreSortParam = String.IsNullOrEmpty(sortOrder) ? "titre_desc" : "";
            ViewBag.DateCreationSortParam = sortOrder == "datecreation" ? "datecreation_desc" : "datecreation";
            switch(sortOrder)
            {
                case "titre_desc":
                    annonces = annonces.OrderByDescending(a => a.Titre).ToList();
                    break;
                case "datecreation":
                    annonces = annonces.OrderBy(a => a.DateCreation).ToList();
                    break;
                case "datecreation_desc":
                    annonces = annonces.OrderByDescending(a => a.DateCreation).ToList();
                    break;
                default:
                    annonces = annonces.OrderBy(a => a.Titre).ToList();
                    break;
            }
            return View(annonces);
        }

        [Route("{id:long}")]
        public ActionResult Details(long id)
        {
            IAnnonceDAO annconceDAO = FabriqueDAO.GetAnnconceDAO();
            Annonce annonce = annconceDAO.FindById(id);
            return View(annonce);
        }

        [Route("add")]
        [Authorize]
        public ActionResult Create(object selectedAuteur = null)
        {
            IAuteurDAO auteurDao = FabriqueDAO.GetAuteurDAO();
            var auteurs = auteurDao.FindAll();
            //ViewBag.AuteurId = new SelectList(auteurs, "AuteurId", "Nom", selectedAuteur);
            ViewBag.AuteurId = new SelectList(auteurs, "AuteurId", "Nom", selectedAuteur);
            return View();
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        [Route("add")]
        public ActionResult Create([Bind(Include = "AnnonceId,Titre,Contenu,EstPublie,AuteurId")]Annonce annonce, string[] tags, IDictionary<string, int> competences)
        {
            annonce.DateCreation = DateTime.Now;
            annonce.MiseAJour = DateTime.Now;
            if (ModelState.IsValid)
            {
                IAnnonceDAO annonceDAO = FabriqueDAO.GetAnnconceDAO();
                annonce = AddTagToAnnonce(annonce, tags);
                annonce = AddCompetenceToAnnonce(annonce, competences);
                bool result = annonceDAO.Ajouter(annonce);
                return RedirectToAction("index");
            }
            else
            {
                return View(annonce);
            }
        }


        [Route("edit/{id}")]
        public ActionResult Edit(long? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Annonce annonce = FabriqueDAO.GetAnnconceDAO().FindById(id);
            PopulateAuteurDropDownList(annonce.AuteurId);
            return View(annonce);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("edit/{id}")]
        public ActionResult Edit([Bind(Include = "AnnonceId,Titre,Contenu,EstPublie,DateCreation,AuteurId")]Annonce annonce, string[] tags)
        {
            annonce.MiseAJour = DateTime.Now;
            //try
            //{
                if (ModelState.IsValid)
                {
                    IAnnonceDAO annonceDAO = FabriqueDAO.GetAnnconceDAO();
                    annonce = AddTagToAnnonce(annonce, tags);
                    bool result = annonceDAO.Edit(annonce);
                    return RedirectToAction("index");
                }
                else
                {
                    return View(annonce);
                }
            //}
            //catch (Exception)
            //{

            //    return RedirectToAction("index");
            //}
        }

        private void PopulateAuteurDropDownList(object selectedAuteur = null)
        {
            IAuteurDAO auteurDao = FabriqueDAO.GetAuteurDAO();
            var auteurs = auteurDao.FindAll();
            ViewBag.AuteurId = new SelectList(auteurs, "AuteurId", "Nom", selectedAuteur);
        }
        private Annonce AddTagToAnnonce(Annonce annonce, string[] tags)
        {
            if (tags != null)
            {
                List<Tag> tagList = new List<Tag>();
                foreach (string tag in tags)
                {
                    tagList.Add(new Tag(tag));
                }
                annonce.Tags = tagList; 
            }
            return annonce;
        }
        private Annonce AddCompetenceToAnnonce(Annonce annonce, IDictionary<string, int> competences)
        {
            ICompetenceDAO competenceDAO = FabriqueDAO.GetComptenceDAO();
            foreach(var key in competences.Keys)
            {
                var competence = competenceDAO.FindOneByDenomination(key);
                AnnonceCompetence annonceCompetence = new AnnonceCompetence();
                annonceCompetence.Competence = competence;
                annonceCompetence.Annonce = annonce;
                annonceCompetence.Niveau = competences[key];
                annonce.AnnonceCompetences.Add(annonceCompetence);
            }
            return annonce;
        }
        [Route("addCompetence")]
        public ActionResult FormAnnonceCompetenceView()
        {
            if (Request.IsAjaxRequest())
            {
                ICompetenceDAO competenceDAO = FabriqueDAO.GetComptenceDAO();
                ViewBag.competenceId = competenceDAO.FindAll();
                
                return PartialView("~/Views/AnnonceCompetence/CreatePartialAnnonceCompetence.cshtml");
                
            }
            return PartialView("Error");
            

        }

    }
}