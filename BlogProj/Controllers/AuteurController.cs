﻿using BlogProj.DAO;
using BlogProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BlogProj.Controllers
{
    [RoutePrefix("auteur")]
    [Route("{action=index}")]
    public class AuteurController : Controller
    {
        // GET: Auteur
        [Route("")]
        public ActionResult Index()
        {
            IAuteurDAO auteurDAO = FabriqueDAO.GetAuteurDAO();
            ICollection<Auteur> auteurs = auteurDAO.FindAll();
            return View(auteurs);
        }

        [Route("{id:long}")]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IAuteurDAO auteurDAO = FabriqueDAO.GetAuteurDAO();
            Auteur auteur = auteurDAO.FindOne(id);
            if(auteur == null)
            {
                return HttpNotFound();
            }
            return View(auteur);
        }
        [Route("{id:long}/annonces")]
        public ActionResult Annonces(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IAnnonceDAO annonceDAO = FabriqueDAO.GetAnnconceDAO();
            var annonces = annonceDAO.FindByAuteurId(id);
           
            return View(annonces);
        }

        [Route("add")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("add")]
        public ActionResult Create([Bind(Include = "AuteurId,Prenom,Nom")]Auteur auteur)
        {
            if (ModelState.IsValid)
            {
                IAuteurDAO auteurDAO = FabriqueDAO.GetAuteurDAO();
                bool result = auteurDAO.Ajouter(auteur);
                return RedirectToAction("index");
            }
            else
            {
                return View(auteur);
            }
        }
    }
}