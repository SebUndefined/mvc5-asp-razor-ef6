﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogProj.Models
{
    public class Auteur
    {
        public long AuteurId { get; set; }
        public string Prenom { get; set; }
        public string Nom { get; set; }

        //Relation
        public ICollection<Annonce> Annonces { get; set; }
    }
}