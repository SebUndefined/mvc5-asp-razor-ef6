﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlogProj.Models
{
    public class Tag
    {
        [Key]
        [StringLength(10)]
        [Index(IsUnique = true)]
        public string Nom { get; set; }

        public Tag()
        {
        }

        public Tag(string nom)
        {
            Nom = nom;
        }

        public ICollection<Annonce> Annonces { get; set; }
    }
}