﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogProj.Models
{
    [Table("AnnonceDb")]
    public class Annonce
    {
        [Key]
        public long AnnonceId { get; set; }
        [Required]
        [StringLength(100)]
        public string Titre { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Contenu { get; set; }
        [Required]
        public DateTime DateCreation { get; set; }
        public DateTime MiseAJour { get; set; }
        [Required]
        public bool EstPublie { get; set; }

        //Relations
        public long AuteurId { get; set; }
        public virtual Auteur Auteur { get; set; }

        public ICollection<Tag> Tags { get; set; }

        public ICollection<AnnonceCompetence> AnnonceCompetences { get; set; }

        public Annonce()
        {
            Tags = new List<Tag>();
            AnnonceCompetences = new List<AnnonceCompetence>();
        }
    }
}