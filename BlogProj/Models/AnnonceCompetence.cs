﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlogProj.Models
{
    public class AnnonceCompetence
    {
        [Key, Column(Order = 0)]
        public long AnnonceId { get; set; }
        public Annonce Annonce { get; set; }

        [Key, Column(Order = 1)]
        public long CompetenceId { get; set; }
        public Competence Competence { get; set; }

        public long Niveau { get; set; }
    }
}