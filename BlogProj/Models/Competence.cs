﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlogProj.Models
{
    public class Competence
    {
        public long CompetenceId { get; set; }
        [StringLength(30)]
        [Index(IsUnique = true)]
        public string Denomination { get; set; }

        public ICollection<AnnonceCompetence> AnnonceCompetences { get; set; }
    }
}